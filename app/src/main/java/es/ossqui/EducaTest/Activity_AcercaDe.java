package es.ossqui.EducaTest;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;

public class Activity_AcercaDe extends AppCompatActivity {

    private Button botonEntrar, botonSalir, botonAtras;
    private ImageView logo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity__acerca_de);

        this.setTitle(R.string.title_activity_about_);

        botonEntrar = findViewById(R.id.buttonEntrarAnimacion);
        botonSalir = findViewById(R.id.buttonSalirAnimacion);
        botonAtras = findViewById(R.id.buttonAtrasLista);


        logo = findViewById(R.id.imageViewLogo);

        //definimos la acción del boton de meter el logo
        botonEntrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //definimos lo que queremos que haga la imagen con botón de entrar
                Animation anim = AnimationUtils.loadAnimation(Activity_AcercaDe.this,R.anim.enter_bottom);
                //iniciamos la acción
                logo.startAnimation(anim);
            }
        });

        //definimos la acción del boton de sacar el logo
        botonSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //definimos lo que queremos que haga la imagen con botón de salir
                Animation anim = AnimationUtils.loadAnimation(Activity_AcercaDe.this,R.anim.exit_bottom);
                //iniciamos la acciónS
                logo.startAnimation(anim);
            }
        });

        botonAtras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }





}
