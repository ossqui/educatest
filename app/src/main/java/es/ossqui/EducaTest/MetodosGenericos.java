package es.ossqui.EducaTest;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.util.Base64;
import android.util.Xml;

import org.xmlpull.v1.XmlSerializer;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;

public class MetodosGenericos {

    //recibe una fila de una tabla y devuelve la pregunta correspondiente
    public static final Pregunta RECUPERAPREGUNTA(Cursor c){
        Pregunta pregunta = new Pregunta();


            //inicializamos la pregunta con los campos obtenidos de la base de datos
            pregunta = new Pregunta(
                    c.getString(c.getColumnIndex("codigo")),
                    c.getString(c.getColumnIndex("enunciado")),
                    c.getString(c.getColumnIndex("categoria")),
                    c.getString(c.getColumnIndex("respuestaCorrecta")),
                    c.getString(c.getColumnIndex("respuestaIncorrecta1")),
                    c.getString(c.getColumnIndex("respuestaIncorrecta2")),
                    c.getString(c.getColumnIndex("respuestaIncorrecta3")),
                    c.getString(c.getColumnIndex("imagen"))
            );

        return pregunta;
    }

    //pasar una imagen a base 64 pero no se usa
    public static String imagen64(Uri uri) {
        String encodedImage = null;
        if (uri != null) {
            Bitmap bm = BitmapFactory.decodeFile(uri.getPath());
            Bitmap resized = Bitmap.createScaledBitmap(bm, 500, 500, true);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            resized.compress(Bitmap.CompressFormat.JPEG, 100, baos);//bmisthebitmapobject
            byte[] b = baos.toByteArray();
            encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
        } else {
            return "";
        }

        return encodedImage;
    }

    //pasar de imagen a string base64
    public static String drawableString(BitmapDrawable drawable) {
        Bitmap bm = drawable.getBitmap();
        String encodedImage = null;
        if (bm != null) {
            Bitmap resized = Bitmap.createScaledBitmap(bm, 500, 500, true);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            resized.compress(Bitmap.CompressFormat.JPEG, 100, baos);//bmisthebitmapobject
            byte[] b = baos.toByteArray();
            encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
        } else {
            return "";
        }
        return encodedImage;
    }

    public static Bitmap StringBitmap(String imageBase64 ){
        byte[] decodedString = Base64.decode(imageBase64, Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        return decodedByte;
    }


    //devuelve el xml de las preguntas en string
    public static String CreateXMLString(Context contexto) throws IllegalArgumentException, IllegalStateException, IOException
    {

        //recuperamos la pregunta
        ArrayList <Pregunta> ListaPreguntas = new ArrayList<Pregunta>();
        ListaPreguntas= Repositorio.recuperarPreguntas(contexto);

        XmlSerializer xmlSerializer = Xml.newSerializer();

        StringWriter writer = new StringWriter();
        xmlSerializer.setOutput(writer);

        //Inicio del documento
        xmlSerializer.startDocument("UTF-8", true);
        xmlSerializer.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true);




        //Etiquetan de apertura de
        xmlSerializer.startTag("", "quiz");

        for (Pregunta p: ListaPreguntas) {
            //Categoria de cada pregunta

            xmlSerializer.startTag("", "question");
            xmlSerializer.attribute("", "type", "category");

            xmlSerializer.startTag("", "category");
            xmlSerializer.startTag("", "text");
            xmlSerializer.text(p.getCategoria());
            xmlSerializer.endTag("", "text");
            xmlSerializer.endTag("", "category");

            xmlSerializer.endTag("", "question");


            xmlSerializer.startTag("", "question");
            xmlSerializer.attribute("", "type", "multichoice");

            xmlSerializer.startTag("", "name");
            xmlSerializer.startTag("", "text");
            xmlSerializer.text(p.getEnunciado());
            xmlSerializer.endTag("", "text");
            xmlSerializer.endTag("", "name");

            xmlSerializer.startTag("","questiontext");
            xmlSerializer.attribute("", "format", "html");
            xmlSerializer.startTag("", "text");
            xmlSerializer.text(p.getEnunciado());
            xmlSerializer.endTag("", "text");
            xmlSerializer.startTag("","file");
            xmlSerializer.attribute("", "name", "imagen_pregunta.jpg");
            xmlSerializer.attribute("", "path", "/");
            xmlSerializer.attribute("", "encoding", "base64");

            if (p.getImagen()!= null) {
                xmlSerializer.text(p.getImagen());
            }else{
                xmlSerializer.text("");
            }
            xmlSerializer.endTag("", "file");
            xmlSerializer.endTag("", "questiontext");

            xmlSerializer.startTag("","answernumbering");
            xmlSerializer.endTag("", "answernumbering");

            xmlSerializer.startTag("","answer");
            xmlSerializer.attribute("","fraction", "100");
            xmlSerializer.attribute("", "format", "html");
            xmlSerializer.startTag("", "text");
            xmlSerializer.text(p.getRespuestaCorrecta());
            xmlSerializer.endTag("", "text");
            xmlSerializer.endTag("", "answer");

            xmlSerializer.startTag("","answer");
            xmlSerializer.attribute("","fraction", "0");
            xmlSerializer.attribute("", "format", "html");
            xmlSerializer.startTag("", "text");
            xmlSerializer.text(p.getRespuestaIncorrecta1());
            xmlSerializer.endTag("", "text");
            xmlSerializer.endTag("", "answer");

            xmlSerializer.startTag("","answer");
            xmlSerializer.attribute("","fraction", "0");
            xmlSerializer.attribute("", "format", "html");
            xmlSerializer.startTag("", "text");
            xmlSerializer.text(p.getRespuestaIncorrecta2());
            xmlSerializer.endTag("", "text");
            xmlSerializer.endTag("", "answer");

            xmlSerializer.startTag("","answer");
            xmlSerializer.attribute("","fraction", "0");
            xmlSerializer.attribute("", "format", "html");
            xmlSerializer.startTag("", "text");
            xmlSerializer.text(p.getRespuestaIncorrecta3());
            xmlSerializer.endTag("", "text");
            xmlSerializer.endTag("", "answer");

            xmlSerializer.endTag("","question");
        }

        //end tag <file>
        xmlSerializer.endTag("","quiz");



        xmlSerializer.endDocument();

        return writer.toString();

    }
}
