package es.ossqui.EducaTest;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


import java.util.ArrayList;

public class Activity_ListadoPreguntas extends AppCompatActivity {

    private static final String LOGTAG = "LogActivity";
    private Context myContext;

    private Button botonAtras;

    private ArrayList <Pregunta> preguntas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        myContext = this;

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity__listadopreguntas);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        botonAtras = findViewById(R.id.buttonAtrasLista);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab_anadir_pregunta);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                  //      .setAction("Action", null).show();
                startActivity(new Intent(Activity_ListadoPreguntas.this, Activity_Pregunta.class));
            }
        });

        botonAtras.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finish();
                    }
                }
        );

    }

    @Override
    protected void onStart() {
        MyLog.d(LOGTAG, "Iniciando onStart");
        super.onStart();
        MyLog.d(LOGTAG,"Finalizando onStart");
    }

    @Override
    protected void onStop() {
        MyLog.d(LOGTAG, "Iniciando onStop");
        super.onStop();
        MyLog.d(LOGTAG,"Finalizando onStop");
    }

    @Override
    protected void onPause() {
        MyLog.d(LOGTAG, "Iniciando onPause");
        super.onPause();
        MyLog.d(LOGTAG,"Finalizando onPause");
    }

    @Override
    protected void onResume() {
        MyLog.d(LOGTAG, "Iniciando onResume");
        super.onResume();

        // Crea una lista con los elementos a mostrar
        preguntas = Repositorio.recuperarPreguntas(myContext);

        TextView txtViewNoPreguntas = (TextView)findViewById(R.id.textViewNoHayPreguntas);

        if (preguntas.isEmpty())
            txtViewNoPreguntas.setVisibility(View.VISIBLE);
        else

            txtViewNoPreguntas.setVisibility(View.INVISIBLE);

        // Inicializa el RecyclerView
        final RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        // Crea el Adaptador con los datos de la lista anterior
        Adapter_Pregunta adaptador = new Adapter_Pregunta(preguntas);


        adaptador.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Acción al pulsar el elemento
                int position = recyclerView.getChildAdapterPosition(v);

                Intent editarpregunta= new Intent(Activity_ListadoPreguntas.this, Activity_Pregunta.class);

                //Con esto enviamos el código de la pregunta al Activity pregunta
                Bundle b = new Bundle();
                b.putInt("getCodigoPregunta", preguntas.get(position).getCodigo());
                editarpregunta.putExtras(b);

                startActivity(editarpregunta);
            }

        });


        // Asocia el Adaptador al RecyclerView
        recyclerView.setAdapter(adaptador);

        // Muestra el RecyclerView en vertical
        recyclerView.setLayoutManager(new LinearLayoutManager(this));



        MyLog.d(LOGTAG,"Finalizando onResume");
    }

    @Override
    protected void onRestart() {
        MyLog.d(LOGTAG, "Iniciando onRestart");
        super.onRestart();
        MyLog.d(LOGTAG,"Finalizando onRestart");
    }

}
