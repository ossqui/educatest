package es.ossqui.EducaTest;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.util.Xml;

import org.xmlpull.v1.XmlSerializer;

import java.io.IOException;
import java.io.StringWriter;
import java.sql.ResultSet;
import java.util.ArrayList;

public class Repositorio {


    //Sirve para insertar una pregunta en nuestra base de datos
    public static boolean insertar(Pregunta p, Context contexto) {

        boolean valor = true;

        //Abrimos la base de datos 'DBPreguntas' en modo escritura
        PregutasSQLiteOpenHelper usdbh = new PregutasSQLiteOpenHelper(contexto, Constantes.NOMBREBD, null, 1);

        SQLiteDatabase db = usdbh.getWritableDatabase();

        //Si hemos abierto correctamente la base de datos
        if (db != null) {
            //Creamos el registro a insertar como objeto ContentValues
            ContentValues valores = new ContentValues();

            valores.put("enunciado", p.getEnunciado());
            valores.put("categoria", p.getCategoria());
            valores.put("respuestaCorrecta", p.getRespuestaCorrecta());
            valores.put("respuestaIncorrecta1", p.getRespuestaIncorrecta1());
            valores.put("respuestaIncorrecta2", p.getRespuestaIncorrecta2());
            valores.put("respuestaIncorrecta3", p.getRespuestaIncorrecta3());
            valores.put("imagen", p.getImagen());

            db.insert(Constantes.TABLAPREGUNTAS, null, valores);

            //cerramos la conexión
            db.close();
        } else {
            valor = false;
        }

        //cerramos la conexión
        db.close();

        return valor;
    }


    //Modifica la pregunta pasada por parametro
    public static boolean modificar(Pregunta p, Context contexto) {
        boolean valor = true;

        //Abrimos la base de datos 'DBUsuarios' en modo escritura
        PregutasSQLiteOpenHelper usdbh = new PregutasSQLiteOpenHelper(contexto, Constantes.NOMBREBD, null, 1);

        SQLiteDatabase db = usdbh.getWritableDatabase();

        //Si hemos abierto correctamente la base de datos
        if (db != null) {

            //Creamos el registro a insertar como objeto ContentValues
            ContentValues valores = new ContentValues();

            valores.put("enunciado", p.getEnunciado());
            valores.put("categoria", p.getCategoria());
            valores.put("respuestaCorrecta", p.getRespuestaCorrecta());
            valores.put("respuestaIncorrecta1", p.getRespuestaIncorrecta1());
            valores.put("respuestaIncorrecta2", p.getRespuestaIncorrecta2());
            valores.put("respuestaIncorrecta3", p.getRespuestaIncorrecta3());
            valores.put("imagen", p.getImagen());


            //Array con los argumentos para la consulta sql
            String[] args = new String[]{Integer.toString(p.getCodigo())};

            //ejecutamos la actualización
            db.update(Constantes.TABLAPREGUNTAS, valores, "codigo=?", args);

            db.close();
        } else {
            valor = false;
        }

        //cerramos la conexión
        db.close();

        return valor;
    }

    //Elimina la pregunta pasada por parametro
    public static boolean eliminar(Pregunta p, Context contexto) {

        boolean valor = true;

        //Abrimos la base de datos 'DBUsuarios' en modo escritura
        PregutasSQLiteOpenHelper usdbh = new PregutasSQLiteOpenHelper(contexto, Constantes.NOMBREBD, null, 1);

        SQLiteDatabase db = usdbh.getWritableDatabase();

        //Si hemos abierto correctamente la base de datos
        if (db != null) {

            String[] args = new String[]{Integer.toString(p.getCodigo())};

            db.delete(Constantes.TABLAPREGUNTAS, "codigo=?", args);

            db.close();
        } else {
            valor = false;
        }

        //cerramos la conexión
        db.close();

        return valor;

    }


    //Recupera la lista completa de preguntas
    public static ArrayList<Pregunta> recuperarPreguntas(Context contexto) {

        ArrayList<Pregunta> listaPreguntas = new ArrayList<Pregunta>();

        //Abrimos la base de datos 'DBPreguntas' en modo escritura
        PregutasSQLiteOpenHelper usdbh = new PregutasSQLiteOpenHelper(contexto, Constantes.NOMBREBD, null, 1);

        SQLiteDatabase db = usdbh.getReadableDatabase();

        if (db != null) {

            Cursor c = db.query(Constantes.TABLAPREGUNTAS,null,null,null,null,null,null);

            //Comprobamos que hay al menos un registro
            if (c.moveToFirst()) {

                do {
                    //Añadimos la pregunta recuperada al listado
                    listaPreguntas.add(MetodosGenericos.RECUPERAPREGUNTA(c));

                //Movemos el cursor a la siguiente pregunta si esta existe
                } while (c.moveToNext());
            }

            //cerramos la conexión
            db.close();
        }

        return listaPreguntas;
    }

    //Recupera la pregunta correspondiente al código pasado por parametro
    public static Pregunta recuperarPregunta(Context contexto, int codigo) {

        Pregunta pregunta = new Pregunta();

        PregutasSQLiteOpenHelper bdsql = new PregutasSQLiteOpenHelper(contexto, Constantes.NOMBREBD, null, 1);

        SQLiteDatabase db = bdsql.getReadableDatabase();

        if(db != null){

            //Array con los argumentos para la consulta sql
            String[] args = new String[]{Integer.toString(codigo)};

            Cursor c = db.query(Constantes.TABLAPREGUNTAS,null,"codigo=?",args,null,null,null);

            //nos vamos al primer y unico registro
            if (c.moveToFirst()) {
                //Recuperamos la pregunta de la base de datos
                pregunta = MetodosGenericos.RECUPERAPREGUNTA(c);
            }
            //cerramos la conexión
            db.close();
        }

        return pregunta;
    }

    //Recupera la lista de categorías
    public static ArrayList<String> listaCategorías(Context contexto) {

        //creamos e inicializamos la colección que contendrá las preguntas
        ArrayList<String> categorías = new ArrayList<String>();

        PregutasSQLiteOpenHelper bdsql = new PregutasSQLiteOpenHelper(contexto, Constantes.NOMBREBD, null, 1);

        SQLiteDatabase db = bdsql.getReadableDatabase();

        if(db != null){
            //campos que se recuperarán de la base de datos, en este caso solo categoría
            String[] campos = new String[] {"categoria"};

            //Ejecutamos la sentencia con el cambio a la anteriores de que el primer campo es el distinct y a demas añadimos los campos a recuperar
            Cursor c = db.query(true,Constantes.TABLAPREGUNTAS,campos,null,null,null,null,null,null);

            //nos vamos al primer y unico registro
            if (c.moveToFirst()) {

                //Recorremos el cursor hasta que no haya más registros
                do {
                    //añadimos la categoría a la colección
                    categorías.add(c.getString(c.getColumnIndex("categoria")));
                } while (c.moveToNext());
            }

            //cerramos la conexión
            db.close();
        }

        return categorías;
    }

    //Devuelve el número de preguntas que hay en la base de datos
    public static String numeroPreguntas(Context contexto) {
        String nPreguntas = "0";

        PregutasSQLiteOpenHelper bdsql = new PregutasSQLiteOpenHelper(contexto, Constantes.NOMBREBD, null, 1);

        SQLiteDatabase db = bdsql.getReadableDatabase();

        Cursor c = db.rawQuery(" SELECT COUNT(*) \"numero\" FROM "+Constantes.TABLAPREGUNTAS, null);

        c.moveToFirst();

        System.out.println(c.getString(c.getColumnIndex("numero")));

        nPreguntas = c.getString(c.getColumnIndex("numero"));

        return nPreguntas;
    }


    // Devuelve el número de categorías distintas que hay en la base de datos
    public static String numeroCategorias(Context contexto) {
        String nCategorias = "0";

        PregutasSQLiteOpenHelper bdsql = new PregutasSQLiteOpenHelper(contexto, Constantes.NOMBREBD, null, 1);
        SQLiteDatabase db = bdsql.getReadableDatabase();

        Cursor c = db.rawQuery(" SELECT COUNT(DISTINCT categoria) \"numero\" FROM "+Constantes.TABLAPREGUNTAS, null);

        c.moveToFirst();

        System.out.println(c.getString(c.getColumnIndex("numero")));

        nCategorias = c.getString(c.getColumnIndex("numero"));

        return nCategorias;
    }




}

