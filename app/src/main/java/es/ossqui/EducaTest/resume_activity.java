package es.ossqui.EducaTest;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.Xml;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;
import org.xmlpull.v1.XmlSerializer;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.ArrayList;

public class resume_activity extends AppCompatActivity {

    private static final String LOGTAG = "LogActivity";

    private Context myContext;

    TextView textViewNPreguntas;
    TextView textViewNCategorias;
    TextView textViewNVersion;
    String versionSDK;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.resume_activity);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        myContext = this;
        textViewNPreguntas = findViewById(R.id.textViewNPreguntas);
        textViewNCategorias = findViewById(R.id.textViewNCategorias);
        textViewNVersion = findViewById(R.id.textViewNVersion);
        this.importandoXML();

        //recupero la versión de android
        versionSDK = Build.VERSION.RELEASE;

    }

    @Override
    public void onBackPressed() {

        Intent a = new Intent(Intent.ACTION_MAIN);
        a.addCategory(Intent.CATEGORY_HOME);
        a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(a);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_resume, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent in;
        switch (item.getItemId()) {
            case R.id.action_listado:
                Log.i("ActionBar", "Listado!");
                in = new Intent(resume_activity.this, Activity_ListadoPreguntas.class);
                startActivity(in);
                return true;
            case R.id.action_acercade:
                Log.i("ActionBar", "Acerca de!");
                ;
                in = new Intent(resume_activity.this, Activity_AcercaDe.class);
                startActivity(in);
                return true;
            case R.id.action_settings:
                Log.i("ActionBar", "Configuracion!");
                ;
                return true;

            case R.id.exportarPreguntas:
                exportarXML();
                ;
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onStart() {
        MyLog.d(LOGTAG, "Iniciando onStart");
        super.onStart();
        MyLog.d(LOGTAG, "Finalizando onStart");
    }

    @Override
    protected void onStop() {
        MyLog.d(LOGTAG, "Iniciando onStop");
        super.onStop();
        MyLog.d(LOGTAG, "Finalizando onStop");
    }

    @Override
    protected void onPause() {
        MyLog.d(LOGTAG, "Iniciando onPause");
        super.onPause();
        MyLog.d(LOGTAG, "Finalizando onPause");
    }

    @Override
    protected void onResume() {
        MyLog.d(LOGTAG, "Iniciando onResume");
        super.onResume();

        //le doy los valores a los textView
        textViewNPreguntas.setText(Repositorio.numeroPreguntas(myContext));
        textViewNCategorias.setText(Repositorio.numeroCategorias(myContext));
        textViewNVersion.setText(versionSDK);
        MyLog.d(LOGTAG, "Finalizando onResume");
    }

    @Override
    protected void onRestart() {
        MyLog.d(LOGTAG, "Iniciando onRestart");
        super.onRestart();
        MyLog.d(LOGTAG, "Finalizando onRestart");
    }


    public void importandoXML(){

        //Creamos las variables de la pregunta que vamos a importar
        Pregunta p;
        String enunciado=null;
        String categoria=null;
        String respuestacorrecta=null;
        String respuestaincorrecta1=null;
        String respuestaincorrecta2=null;
        String respuestaincorrecta3=null;
        String imagen=null;

        String text= null;
        int contador=0;

        //Recibimos el intent
        Intent receivedIntent = getIntent();

        //Si el intent es distinto de null
        if(receivedIntent != null) {
            //Recogemos la accion del intent
            String receivedAction = receivedIntent.getAction();

            //Si la accion del inten es igual a android.intent.action.SEND
            //Se ejecutará la lectura del archivo
            if(receivedAction == "android.intent.action.SEND"){

                Uri data = receivedIntent.getParcelableExtra(Intent.EXTRA_STREAM);

                try {

                    InputStream fis = getContentResolver().openInputStream(data);
                    XmlPullParserFactory xppf = XmlPullParserFactory.newInstance();
                    xppf.setNamespaceAware(false);
                    XmlPullParser parser = xppf.newPullParser();
                    parser.setInput(fis, null);

                    parser.nextTag();
                    parser.require(XmlPullParser.START_TAG, null, "quiz");

                    //Leyendo el documento

                    int act;
                    String tag="";
                    boolean enterQuestion=false;
                    int contadorRespuestas= 0;




                    while((act=parser.next()) != XmlPullParser.END_DOCUMENT) {

                        switch (act) {
                            case XmlPullParser.START_TAG:

                                tag = parser.getName();
                                Log.d("CLIENTE RSS: ",tag);
                                if(tag.equals("question"))
                                {enterQuestion=true;}
                                break;

                            case XmlPullParser.TEXT:
                                if(tag.equals("text"))
                                {

                                    if(contadorRespuestas==0){
                                        categoria= parser.getText();
                                        contadorRespuestas++;
                                    }
                                    else if(contadorRespuestas==1){
                                        enunciado= parser.getText();
                                        contadorRespuestas++;

                                    }
                                    else if(contadorRespuestas==2){

                                        contadorRespuestas++;
                                    }

                                    else if(contadorRespuestas==3){
                                        respuestacorrecta= parser.getText();
                                        contadorRespuestas++;

                                    }
                                    else if(contadorRespuestas==4){
                                        respuestaincorrecta1= parser.getText();
                                        contadorRespuestas++;

                                    }
                                    else if(contadorRespuestas==5){
                                        respuestaincorrecta2= parser.getText();
                                        contadorRespuestas++;

                                    }
                                    else if(contadorRespuestas==6){
                                        respuestaincorrecta3= parser.getText();
                                        contadorRespuestas++;

                                        //Como es el último dato que recuperamos de la pregunta la añadimos a la base de datos
                                        Pregunta nuevaPregunta= new Pregunta(enunciado,categoria,respuestacorrecta,respuestaincorrecta1,respuestaincorrecta2,respuestaincorrecta3, imagen);
                                        Repositorio.insertar(nuevaPregunta,myContext);
                                        System.out.println("Pregunta Añadida correctamente a la base de datos......................................");
                                        contadorRespuestas=0;

                                    }
                                    else{
                                        System.out.println("Error al leer");
                                    }

                                }

                                if(tag.equals("file"))
                                {
                                    //si la imagen es una cadena vacia se guardará en null la foto sino se recupera del xml
                                    if (parser.getText()!=""){
                                        imagen= parser.getText();
                                    }else{
                                        imagen=null;
                                    }

                                }


                                tag="";
                                break;

                            case XmlPullParser.END_TAG:
                                if(parser.getName().equals("question"))
                                {
                                    //System.out.println("terminado");
                                    //contadorRespuestas=0;
                                    //System.out.println("cONTADOR EN TERMINADO: "+ contadorRespuestas);
                                }
                                break;
                        }

                    }



                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (XmlPullParserException e) {
                    e.printStackTrace();
                }
            }
        }


    }

    //Crea un ducumento xml en la memoria del dispositivo
    private void exportarXML(){


        //crea un archivo en la ruta pregntaExportar en la memoria del dispositivo
        String ruta = Environment.getExternalStorageDirectory().toString();

        File directorio = new File(ruta + "/preguntasExportar");

        String fname = "Lista_Preguntas.xml";

        File file = new File (directorio, fname);

        try
        {

            //comprobamos si el directorio no existe para crearlo
            if (!directorio.exists()) {
                directorio.mkdirs();
            }

            //comprueba si el archivo no existe para eliminarlo.
            if (file.exists ()) {
                file.delete();
            }

            //abrilos la lectura y escribimos el fichero
            FileWriter fw=new FileWriter(file);

            fw.write(MetodosGenericos.CreateXMLString(myContext));


            //Cerramos la lectura
            fw.close();

        }
        catch (Exception ex)
        {
            MyLog.e("Ficheros", "Error de escritura del fichero en la memoria del dispositivo");
        }




        //obtenemos al ruta absoluta del fichero
        String cadena = directorio.getAbsolutePath()+"/"+fname;

        Uri path = Uri.parse("file://"+cadena);

        //configuramos el envio del correo con los datos
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto","aosomoza@iesfranciscodelosrios.es", null));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Preguntas exportadas de EducaTest");
        emailIntent.putExtra(Intent.EXTRA_TEXT, "El archivo adjunto contiene las preguntas.");
        emailIntent .putExtra(Intent.EXTRA_STREAM, path);
        startActivity(Intent.createChooser(emailIntent, "enviar email..."));
    }

}
