package es.ossqui.EducaTest;

public class Pregunta {

    private int codigo;
    private String enunciado;
    private String categoria;
    private String respuestaCorrecta;
    private String RespuestaIncorrecta1;
    private String RespuestaIncorrecta2;
    private String RespuestaIncorrecta3;
    private String imagen;

    public Pregunta (){
        this.codigo = 0;
        this.enunciado = "";
        this.categoria = "";
        this.respuestaCorrecta = "";
        this.RespuestaIncorrecta1 = "";
        this.RespuestaIncorrecta2 = "";
        this.RespuestaIncorrecta3 = "";
        this.imagen = "";
    }

    public Pregunta(String codigo, String enunciado, String categoria, String respuestaCorrecta, String RespuestaIncorrecta1, String RespuestaIncorrecta2, String RespuestaIncorrecta3, String imagen) {
        this.codigo = Integer.parseInt(codigo);
        this.enunciado = enunciado;
        this.categoria = categoria;
        this.respuestaCorrecta = respuestaCorrecta;
        this.RespuestaIncorrecta1 = RespuestaIncorrecta1;
        this.RespuestaIncorrecta2 = RespuestaIncorrecta2;
        this.RespuestaIncorrecta3 = RespuestaIncorrecta3;
        this.imagen = imagen;
    }
    public Pregunta(int codigo, String enunciado, String categoria, String respuestaCorrecta, String RespuestaIncorrecta1, String RespuestaIncorrecta2, String RespuestaIncorrecta3) {
        this.codigo = codigo;
        this.enunciado = enunciado;
        this.categoria = categoria;
        this.respuestaCorrecta = respuestaCorrecta;
        this.RespuestaIncorrecta1 = RespuestaIncorrecta1;
        this.RespuestaIncorrecta2 = RespuestaIncorrecta2;
        this.RespuestaIncorrecta3 = RespuestaIncorrecta3;
        this.imagen = imagen;
    }
    public Pregunta(String enunciado, String categoria, String respuestaCorrecta, String RespuestaIncorrecta1, String RespuestaIncorrecta2, String RespuestaIncorrecta3, String imagen) {
        this.enunciado = enunciado;
        this.categoria = categoria;
        this.respuestaCorrecta = respuestaCorrecta;
        this.RespuestaIncorrecta1 = RespuestaIncorrecta1;
        this.RespuestaIncorrecta2 = RespuestaIncorrecta2;
        this.RespuestaIncorrecta3 = RespuestaIncorrecta3;
        this.imagen = imagen;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getEnunciado() {
        return enunciado;
    }

    public void setEnunciado(String enunciado) {
        this.enunciado = enunciado;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getRespuestaCorrecta() {
        return respuestaCorrecta;
    }

    public void setRespuestaCorrecta(String respuestaCorrecta) {
        this.respuestaCorrecta = respuestaCorrecta;
    }

    public String getRespuestaIncorrecta1() {
        return RespuestaIncorrecta1;
    }

    public void setRespuestaIncorrecta1(String respuestaIncorrecta1) {
        this.RespuestaIncorrecta1 = respuestaIncorrecta1;
    }

    public String getRespuestaIncorrecta2() {
        return RespuestaIncorrecta2;
    }

    public void setRespuestaIncorrecta2(String getRespuestaIncorrecta2) {
        this.RespuestaIncorrecta2 = getRespuestaIncorrecta2;
    }

    public String getRespuestaIncorrecta3() {
        return RespuestaIncorrecta3;
    }

    public void setRespuestaIncorrecta3(String respuestaIncorrecta3) {
        this.RespuestaIncorrecta3 = respuestaIncorrecta3;
    }
}
