package es.ossqui.EducaTest;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Activity_Pregunta extends AppCompatActivity {
    final private int CODE_WRITE_EXTERNAL_STORAGE_PERMISSION = 123;
    final private int CODE_WRITE_EXTERNAL_STORAGE_PERMISSION_CAMARA = 234;
    final private int CODE_WRITE_EXTERNAL_STORAGE_PERMISSION_GALERIA = 334;
    private Context myContext;
    private ConstraintLayout constraintLayoutActivityPregunta;
    private Spinner spinner;
    private ArrayAdapter<String> adapter;
    private Button button;
    int codigo;

    private String TAG = "imagenesPreguntas";
    private static final int REQUEST_CAPTURE_IMAGE = 200;
    private static final int REQUEST_SELECT_IMAGE = 201;
    final String pathFotos = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/fotosPreguntas/";
    private Uri uri;
    private Button botonFoto;
    private Button botonElimnaImagen;
    private Button botonAtras;

    private static final String LOGTAG = "LogActivity";

    private ImageView imageView;
    private String imagenGaleria;

    private boolean edita;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity__pregunta);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        edita = false;

        final EditText enunciado = (EditText) findViewById(R.id.editTextPregunta);
        final EditText respuestaCorrecta = (EditText) findViewById(R.id.editTextRespuestaCorrecta);
        final EditText respuestaIncorrecta1 = (EditText) findViewById(R.id.editTextRespuestaIncorrecta1);
        final EditText respuestaIncorrecta2 = (EditText) findViewById(R.id.editTextRespuestaIncorrecta2);
        final EditText respuestaIncorrecta3 = (EditText) findViewById(R.id.editTextRespuestaIncorrecta3);
        final Spinner spinner = (Spinner) findViewById(R.id.spinnerCategoria);
        final Button buttoneliminar = findViewById(R.id.buttoneliminar);
        botonAtras = findViewById(R.id.buttonAtrasPregunta);
        imageView = findViewById(R.id.imageViewFoto);
        imagenGaleria = null;
        imageView.setRotation(0);

        //tomar foto
        botonFoto = (Button) findViewById(R.id.btnTomaFoto);


        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            botonFoto.setEnabled(false);
        } else {
            botonFoto.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    int WriteExternalStoragePermission = ContextCompat.checkSelfPermission(myContext, Manifest.permission.WRITE_EXTERNAL_STORAGE);
                    MyLog.d("MainActivity", "WRITE_EXTERNAL_STORAGE Permission: " + WriteExternalStoragePermission);

                    if (WriteExternalStoragePermission != PackageManager.PERMISSION_GRANTED) {
                        // Permiso denegado
                        // A partir de Marshmallow (6.0) se pide aceptar o rechazar el permiso en tiempo de ejecución
                        // En las versiones anteriores no es posible hacerlo
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            ActivityCompat.requestPermissions(Activity_Pregunta.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, CODE_WRITE_EXTERNAL_STORAGE_PERMISSION_CAMARA);
                            // Una vez que se pide aceptar o rechazar el permiso se ejecuta el método "onRequestPermissionsResult" para manejar la respuesta
                            // Si el usuario marca "No preguntar más" no se volverá a mostrar este diálogo
                        } else {
                            Snackbar.make(constraintLayoutActivityPregunta, getResources().getString(R.string.write_permission_denied), Snackbar.LENGTH_LONG)
                                    .show();
                        }
                    } else {
                        // Permiso aceptado
                        if (getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
                            takePicture();
                            imageView.setRotation(90);
                        }

                    }

                }
            });
        }


        buttoneliminar.setEnabled(false);

        // Almacenamos el contexto de la actividad para utilizar en las clases internas
        myContext = this;

        ArrayList<String> items = Repositorio.listaCategorías(myContext);

        adapter = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, items);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);

        // Definición del Spinner

        spinner.setAdapter(adapter);

        // Definición de la acción del botón
        button = (Button) findViewById(R.id.buttonAnadir);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Recuperación de la vista del AlertDialog a partir del layout de la Actividad
                LayoutInflater layoutActivity = LayoutInflater.from(myContext);
                View viewAlertDialog = layoutActivity.inflate(R.layout.alert_dialog, null);

                // Definición del AlertDialog
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(myContext);

                // Asignación del AlertDialog a su vista
                alertDialog.setView(viewAlertDialog);

                // Recuperación del EditText del AlertDialog
                final EditText dialogInput = (EditText) viewAlertDialog.findViewById(R.id.dialogInput);

                // Configuración del AlertDialog
                alertDialog
                        .setCancelable(false)
                        // Botón Añadir
                        .setPositiveButton(getResources().getString(R.string.add),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialogBox, int id) {
                                        adapter.add(dialogInput.getText().toString());
                                        spinner.setSelection(adapter.getPosition(dialogInput.getText().toString()));
                                    }
                                })
                        // Botón Cancelar
                        .setNegativeButton(getResources().getString(R.string.cancel),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialogBox, int id) {
                                        dialogBox.cancel();
                                    }
                                })
                        .create()
                        .show();
            }
        });


        //Boton eliminar
        //inicializamos el boton eliminar
        botonElimnaImagen = (Button) findViewById(R.id.btnEliminarImagen);

        //Creamos las acciónes sobre el botón
        botonElimnaImagen.setOnClickListener(new View.OnClickListener() {

            //creamos la acción de hacer click
            @Override
            public void onClick(View v) {
                imageView.setImageResource(android.R.color.transparent);
                imageView.setImageResource(0);
                imageView.setRotation(0);
            }
        });

        // Definición de la acción del botón
        button = (Button) findViewById(R.id.buttoneliminar);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Pregunta p = new Pregunta(codigo, enunciado.getText().toString(), spinner.getSelectedItem().toString(), respuestaCorrecta.getText().toString(), respuestaIncorrecta1.getText().toString(), respuestaIncorrecta2.getText().toString(), respuestaIncorrecta3.getText().toString());

                Repositorio.eliminar(p, myContext);
                finish();
            }
        });

        botonAtras.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finish();
                    }
                }
        );

        Button buttonGallery = (Button) findViewById(R.id.buttonGallery);
        buttonGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int WriteExternalStoragePermission = ContextCompat.checkSelfPermission(myContext, Manifest.permission.WRITE_EXTERNAL_STORAGE);
                MyLog.d("MainActivity", "WRITE_EXTERNAL_STORAGE Permission: " + WriteExternalStoragePermission);

                if (WriteExternalStoragePermission != PackageManager.PERMISSION_GRANTED) {
                    // Permiso denegado
                    // A partir de Marshmallow (6.0) se pide aceptar o rechazar el permiso en tiempo de ejecución
                    // En las versiones anteriores no es posible hacerlo
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        ActivityCompat.requestPermissions(Activity_Pregunta.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, CODE_WRITE_EXTERNAL_STORAGE_PERMISSION_GALERIA);
                        // Una vez que se pide aceptar o rechazar el permiso se ejecuta el método "onRequestPermissionsResult" para manejar la respuesta
                        // Si el usuario marca "No preguntar más" no se volverá a mostrar este diálogo
                    } else {
                        Snackbar.make(constraintLayoutActivityPregunta, getResources().getString(R.string.write_permission_denied), Snackbar.LENGTH_LONG)
                                .show();
                    }
                } else {
                    // Permiso aceptado
                    selectPicture();
                    imageView.setRotation(90);

                }

            }
        });


        Pregunta p;


        // Recibimos el código de la pregunta del listado de preguntas
        Bundle bundle = this.getIntent().getExtras();


        if (bundle != null) {
            this.codigo = bundle.getInt("getCodigoPregunta");
                imageView.setRotation(0);

            // recuperamos una pregunta de la base de datos.
            p = Repositorio.recuperarPregunta(myContext, codigo);
            if (p != null) {
                edita = true;
                enunciado.setText(p.getEnunciado());
                respuestaCorrecta.setText(p.getRespuestaCorrecta());
                respuestaIncorrecta1.setText(p.getRespuestaIncorrecta1());
                respuestaIncorrecta2.setText(p.getRespuestaIncorrecta2());
                respuestaIncorrecta3.setText(p.getRespuestaIncorrecta3());
                spinner.setSelection(items.indexOf(p.getCategoria()));

                //Compruebasi la pregunta recuperada tiene imagen o no imagen
                if (p.getImagen() != null) {
                    //si la tiene, la asigna al contenedor de la vista y la rota 90 grados para que se vea derecha
                    imageView.setImageBitmap(MetodosGenericos.StringBitmap(p.getImagen()));
                    imageView.setRotation(90);
                }else {
                    //si no tiene imagen no la gira
                    imageView.setRotation(0);
                }
                buttoneliminar.setEnabled(true);
            }

        }


        // Recuperamos el Layout donde mostrar el Snackbar con las notificaciones
        constraintLayoutActivityPregunta = findViewById(R.id.constraintPregunta);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //Pide permiso de escritura
                int WriteExternalStoragePermission = ContextCompat.checkSelfPermission(myContext, Manifest.permission.WRITE_EXTERNAL_STORAGE);
                MyLog.d("MainActivity", "WRITE_EXTERNAL_STORAGE Permission: " + WriteExternalStoragePermission);

                if (WriteExternalStoragePermission != PackageManager.PERMISSION_GRANTED) {
                    // Permiso denegado
                    // A partir de Marshmallow (6.0) se pide aceptar o rechazar el permiso en tiempo de ejecución
                    // En las versiones anteriores no es posible hacerlo
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        ActivityCompat.requestPermissions(Activity_Pregunta.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, CODE_WRITE_EXTERNAL_STORAGE_PERMISSION);
                        // Una vez que se pide aceptar o rechazar el permiso se ejecuta el método "onRequestPermissionsResult" para manejar la respuesta
                        // Si el usuario marca "No preguntar más" no se volverá a mostrar este diálogo
                    } else {
                        Snackbar.make(constraintLayoutActivityPregunta, getResources().getString(R.string.write_permission_denied), Snackbar.LENGTH_LONG)
                                .show();
                    }
                } else {
                    // Permiso aceptado
                    Snackbar.make(constraintLayoutActivityPregunta, getResources().getString(R.string.write_permission_granted), Snackbar.LENGTH_LONG)
                            .show();

                }

                BitmapDrawable drawable = (BitmapDrawable) imageView.getDrawable();
                //Ste snackbar se muestra cuando algun campo no se ha rellenado
                if (
                        enunciado.getText().toString().isEmpty() ||
                                respuestaCorrecta.getText().toString().isEmpty() ||
                                respuestaIncorrecta1.getText().toString().isEmpty() ||
                                respuestaIncorrecta2.getText().toString().isEmpty() ||
                                respuestaIncorrecta3.getText().toString().isEmpty() ||
                                spinner.getSelectedItem() == null
                        ) {
                    Snackbar.make(view, "Rellena todos los campos", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                    //pone en rojo el campo que falta.
                    view.clearFocus();

                    //cierrra el teclado para que se pueda ver el snackbar.
                    if (view != null) {
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    }

                } else {
                    if (edita) {
                        //comprueba si imagen

                        if (drawable != null) {
                            //Se le asigna la imagen a la variable que se le pasará a la base de datos
                            imagenGaleria = MetodosGenericos.drawableString(drawable);

                        } else {
                            imagenGaleria = null;
                        }
                        Pregunta p = new Pregunta(Integer.toString(codigo), enunciado.getText().toString(), spinner.getSelectedItem().toString(), respuestaCorrecta.getText().toString(), respuestaIncorrecta1.getText().toString(), respuestaIncorrecta2.getText().toString(), respuestaIncorrecta3.getText().toString(), imagenGaleria);
                        Repositorio.modificar(p, myContext);
                        edita = false;
                        finish();
                    } else {
                        if (drawable != null) {
                            //Se le asigna la imagen a la variable que se le pasará a la base de datos
                            imagenGaleria = MetodosGenericos.drawableString(drawable);
                            imageView.setRotation(90);

                        } else {
                            imagenGaleria = null;
                            imageView.setRotation(0);
                        }
                        Pregunta p = new Pregunta(enunciado.getText().toString(), spinner.getSelectedItem().toString(), respuestaCorrecta.getText().toString(), respuestaIncorrecta1.getText().toString(), respuestaIncorrecta2.getText().toString(), respuestaIncorrecta3.getText().toString(), imagenGaleria);
                        Repositorio.insertar(p, myContext);
                        finish();
                    }
                }

            }


        });
    }


    private void selectPicture() {
        // Se le pide al sistema una imagen del dispositivo
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, getResources().getString(R.string.choose_picture)),REQUEST_SELECT_IMAGE);
        imageView.setRotation(90);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {

            case (REQUEST_CAPTURE_IMAGE):
                if (resultCode == Activity.RESULT_OK) {
                    // Se carga la imagen desde un objeto URI al imageView

                    imageView.setImageURI(uri);


                    // Se le envía un broadcast a la Galería para que se actualice
                    Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                    mediaScanIntent.setData(uri);
                    sendBroadcast(mediaScanIntent);
                } else if (resultCode == Activity.RESULT_CANCELED) {
                    // Se borra el archivo temporal
                    File file = new File(uri.getPath());
                    file.delete();
                }
                break;

            case (REQUEST_SELECT_IMAGE):
                if (resultCode == Activity.RESULT_OK) {
                    // Se carga la imagen desde un objeto Bitmap
                    Uri selectedImage = data.getData();
                    String selectedPath = selectedImage.getPath();

                    if (selectedPath != null) {
                        // Se leen los bytes de la imagen
                        InputStream imageStream = null;
                        try {
                            imageStream = getContentResolver().openInputStream(selectedImage);
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }

                        // Se transformam los bytes de la imagen a un Bitmap
                        Bitmap bmp = BitmapFactory.decodeStream(imageStream);

                        // Se carga el Bitmap en el ImageView
                        ImageView imageView = findViewById(R.id.imageViewFoto);
                        imageView.setImageBitmap(bmp);
                    }
                }
                break;
        }
    }


    private void takePicture() {
        try {
            // Se crea el directorio para las fotografías
            File dirFotos = new File(pathFotos);
            dirFotos.mkdirs();

            // Se crea el archivo para almacenar la fotografía
            File fileFoto = File.createTempFile(getFileCode(), ".jpg", dirFotos);

            // Se crea el objeto Uri a partir del archivo
            // A partir de la API 24 se debe utilizar FileProvider para proteger
            // con permisos los archivos creados
            // Con estas funciones podemos evitarlo
            // https://stackoverflow.com/questions/42251634/android-os-fileuriexposedexception-file-jpg-exposed-beyond-app-through-clipdata
            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
            StrictMode.setVmPolicy(builder.build());
            uri = Uri.fromFile(fileFoto);
            Log.d(TAG, uri.getPath().toString());

            // Se crea la comunicación con la cámara
            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            // Se le indica dónde almacenar la fotografía
            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
            // Se lanza la cámara y se espera su resultado
            startActivityForResult(cameraIntent, REQUEST_CAPTURE_IMAGE);

        } catch (IOException ex) {

            Log.d(TAG, "Error: " + ex);
            CoordinatorLayout coordinatorLayout = findViewById(R.id.coordinatorLayout);
            Snackbar snackbar = Snackbar
                    .make(coordinatorLayout, getResources().getString(R.string.error_files), Snackbar.LENGTH_LONG);
            snackbar.show();
        }
        imageView.setRotation(90);
    }

    private String getFileCode() {
        // Se crea un código a partir de la fecha y hora
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyymmddhhmmss", java.util.Locale.getDefault());
        String date = dateFormat.format(new Date());
        // Se devuelve el código
        return "pic_" + date;
    }

    //permiso de almacenamiento
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case CODE_WRITE_EXTERNAL_STORAGE_PERMISSION:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permiso aceptado
                    Snackbar.make(constraintLayoutActivityPregunta, getResources().getString(R.string.write_permission_accepted), Snackbar.LENGTH_LONG)
                            .show();
                } else {
                    // Permiso rechazado
                    Snackbar.make(constraintLayoutActivityPregunta, getResources().getString(R.string.write_permission_not_accepted), Snackbar.LENGTH_LONG)
                            .show();
                }
                break;
            case CODE_WRITE_EXTERNAL_STORAGE_PERMISSION_CAMARA:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permiso aceptado
                    if (getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
                        takePicture();
                    }
                } else {
                    // Permiso rechazado
                    Snackbar.make(constraintLayoutActivityPregunta, getResources().getString(R.string.write_permission_not_accepted), Snackbar.LENGTH_LONG)
                            .show();
                }
                break;
            case CODE_WRITE_EXTERNAL_STORAGE_PERMISSION_GALERIA:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permiso aceptado
                    selectPicture();


                } else {
                    // Permiso rechazado
                    Snackbar.make(constraintLayoutActivityPregunta, getResources().getString(R.string.write_permission_not_accepted), Snackbar.LENGTH_LONG)
                            .show();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }


}
