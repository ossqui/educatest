package es.ossqui.EducaTest;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.util.Timer;
import java.util.TimerTask;

public class LogActivity extends AppCompatActivity {

    private static final String LOGTAG = "LogActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        MyLog.d(LOGTAG,"Iniciando onCreate");
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                startActivity(new Intent(getApplicationContext(), resume_activity.class));
            }
        }, 1000);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log);
        MyLog.d(LOGTAG,"Finalizando onCreate");
    }

    @Override
    protected void onStart() {
        MyLog.d(LOGTAG, "Iniciando onStart");
        super.onStart();
        MyLog.d(LOGTAG,"Finalizando onStart");
    }

    @Override
    protected void onStop() {
        MyLog.d(LOGTAG, "Iniciando onStop");
        super.onStop();
        MyLog.d(LOGTAG,"Finalizando onStop");
    }

    @Override
    protected void onPause() {
        MyLog.d(LOGTAG, "Iniciando onPause");
        super.onPause();
        MyLog.d(LOGTAG,"Finalizando onPause");
    }

    @Override
    protected void onResume() {
        MyLog.d(LOGTAG, "Iniciando onResume");
        super.onResume();
        MyLog.d(LOGTAG,"Finalizando onResume");
    }

    @Override
    protected void onRestart() {
        MyLog.d(LOGTAG, "Iniciando onRestart");
        super.onRestart();
        MyLog.d(LOGTAG,"Finalizando onRestart");
    }
}
